package ProjetoPDCase.teste.rest;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import ProjetoPDCase.teste.data.ProcedimentoRepository;
import ProjetoPDCase.teste.model.Procedimento;

@Path("/procedimento")
public class ProcedimentoRESTService {
    
    @Inject
    private ProcedimentoRepository repository;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/listar")
    public List<Procedimento> listAllProcedimentos() {
        return repository.listar();
    }

    @GET
    @Path("/buscar/{id:[0-9][0-9]*}")
    @Produces(MediaType.APPLICATION_JSON)
    public Procedimento buscarProcedimento(@PathParam("id") long id) {
        Procedimento Procedimento = repository.buscar(id);
        if (Procedimento == null) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        return Procedimento;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/excluir")
    public Response excluirProcedimento(Procedimento procedimento) {
        repository.excluir(procedimento);
        return Response.ok().build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/salvar")
    public Response salvarProcedimento(Procedimento procedimento) {
        repository.salvar(procedimento);
        return Response.ok().build();
    }


}
