package ProjetoPDCase.teste.rest;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import ProjetoPDCase.teste.data.PessoaRepository;
import ProjetoPDCase.teste.data.PlanoSaudeRepository;
import ProjetoPDCase.teste.data.ProcedimentoRepository;
import ProjetoPDCase.teste.model.Pessoa;
import ProjetoPDCase.teste.model.PlanoSaude;
import ProjetoPDCase.teste.model.Procedimento;
import ProjetoPDCase.teste.model.Resposta;

@Path("/pessoa")
public class PessoaRESTService {
    
    @Inject
    private PessoaRepository repository;

    @Inject
    private PlanoSaudeRepository repositoryPlanoSaude;
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/listar")
    public List<Pessoa> listAllPessoas() {
        return repository.listar();
    }

    @GET
    @Path("/buscar/{id:[0-9][0-9]*}")
    @Produces(MediaType.APPLICATION_JSON)
    public Pessoa buscarPessoa(@PathParam("id") long id) {
        Pessoa pessoa = repository.buscar(id);
        if (pessoa == null) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        return pessoa;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/salvar")
    public Response salvarPessoa(Pessoa pessoa) {
    	
    	repository.salvar(pessoa);
        return Response.ok().build();
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/excluir")
    public Response excluirPessoa(Pessoa pessoa) {
        repository.excluir(pessoa);
        return Response.ok().build();
    }
    
    @GET
    @Path("/verificarProcedimento/{idPessoa}/{idProcedimento}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response verificarProcedimentoBeneficiario(@PathParam("idPessoa") Long idPessoa, @PathParam("idProcedimento") Long idProcedimento) {
    	
    	Pessoa pessoa = repository.buscar(idPessoa);
    	PlanoSaude planoSaude = repositoryPlanoSaude.buscar(pessoa.getPlanoSaude().getIdPlano()) ;
    	
    	for (Procedimento procedimento : planoSaude.getListaProcedimento()) {
    		if(procedimento.getId().equals(idProcedimento)) {
    			return Response.ok(new Resposta(200, "SUCESSO. O profissional esta liberado para realizar o procedimento selecionado")).build();
    		}
		}
    	return Response.ok(new Resposta(200, "Ops !! Infelizmente o plano do profissional não cobre o procedimento solicitado")).build();
    }


}
