package ProjetoPDCase.teste.rest;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import ProjetoPDCase.teste.data.PlanoSaudeRepository;
import ProjetoPDCase.teste.model.PlanoSaude;

@Path("/planoSaude")
public class PlanoSaudeRESTService {
    
    @Inject
    private PlanoSaudeRepository repository;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/listar")
    public List<PlanoSaude> listAllPlanoSaudes() {
        return repository.listar();
    }

    @GET
    @Path("/buscar/{id:[0-9][0-9]*}")
    @Produces(MediaType.APPLICATION_JSON)
    public PlanoSaude buscarPlanoSaude(@PathParam("id") long id) {
        PlanoSaude planoSaude = repository.buscar(id);
        if (planoSaude == null) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        return planoSaude;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/salvar")
    public Response salvarPlanoSaude(PlanoSaude planoSaude) {
        repository.salvar(planoSaude);
        return Response.ok().build();
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/excluir")
    public Response excluirPlanoSaude(PlanoSaude planoSaude) {
        repository.excluir(planoSaude);
        return Response.ok().build();
    }

}
