package ProjetoPDCase.teste.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "planoSaude")
public class PlanoSaude implements Serializable {
    
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
	private Long idPlano;

    private String nome;
    
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<Procedimento> listaProcedimento;

    public PlanoSaude() {
    }
    
	public Long getIdPlano() {
		return idPlano;
	}

	public void setIdPlano(Long idPlano) {
		this.idPlano = idPlano;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Procedimento> getListaProcedimento() {
		return listaProcedimento;
	}

	public void setListaProcedimento(List<Procedimento> listaProcedimento) {
		this.listaProcedimento = listaProcedimento;
	}
	
}
