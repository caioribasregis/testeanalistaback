/*
 * JBoss, Home of Professional Open Source
 * Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ProjetoPDCase.teste.data;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import ProjetoPDCase.teste.model.Procedimento;

@Stateless
@LocalBean
public class ProcedimentoRepository {

	@PersistenceContext
	private EntityManager em;

	public Procedimento buscar(Long id) {
		return em.find(Procedimento.class, id);
	}

	public void excluir(Procedimento procedimento) {
		em.remove(procedimento);
	}

	public List<Procedimento> listar() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Procedimento> criteria = cb.createQuery(Procedimento.class);
		Root<Procedimento> procedimento = criteria.from(Procedimento.class);
		criteria.select(procedimento).orderBy(cb.asc(procedimento.get("nome")));
		return em.createQuery(criteria).getResultList();
	}

	
	public void salvar(Procedimento procedimento) {
		em.merge(procedimento);
	}
}
