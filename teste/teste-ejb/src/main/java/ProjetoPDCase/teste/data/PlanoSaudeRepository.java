/*
 * JBoss, Home of Professional Open Source
 * Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ProjetoPDCase.teste.data;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import ProjetoPDCase.teste.model.PlanoSaude;

@Stateless
@LocalBean
public class PlanoSaudeRepository {

	@PersistenceContext
	private EntityManager em;

	public PlanoSaude buscar(Long id) {
		return em.find(PlanoSaude.class, id);
	}
	
	public void excluir(PlanoSaude planoSaude) {
		em.remove(planoSaude);
	}

	public List<PlanoSaude> listar() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<PlanoSaude> criteria = cb.createQuery(PlanoSaude.class);
		Root<PlanoSaude> PlanoSaude = criteria.from(PlanoSaude.class);
		criteria.select(PlanoSaude).orderBy(cb.asc(PlanoSaude.get("nome")));
		return em.createQuery(criteria).getResultList();
	}

	
	public void salvar(PlanoSaude PlanoSaude) {
		em.merge(PlanoSaude);
	}
}
